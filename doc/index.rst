`aiobme280` is a Python 3 module to read data asynchronously from BME280
environmental sensor.

Table of Contents
-----------------

.. toctree::
   intro
   conf
   use
   api
   changelog

* :ref:`genindex`
* :ref:`search`

.. vim: sw=4:et:ai
