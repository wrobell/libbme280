API
===
.. autoclass:: aiobme280.Sensor
    :members:
    :special-members:

.. vim: sw=4:et:ai
